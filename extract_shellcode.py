#!/usr/bin/python

import os, sys, string

if len(sys.argv) != 2:
    print "Usage: "+sys.argv[0]+" <shell_executable>"
    exit(1)


def ishex( s ):
    for c in s:
        if c not in string.hexdigits:
            return False
    return True

shell_exec = sys.argv[1]
dump = os.popen( "objdump -d " + shell_exec ).readlines()


print "-"*64
l = 0
for line in dump:
    for elt in line.split():
        if len( elt ) == 2 and ishex( elt ):
            l += 1
            sys.stdout.write( "\\x"+elt )
print "\n"+"-"*64
print "Shellcode is "+str(l)+" bytes"
