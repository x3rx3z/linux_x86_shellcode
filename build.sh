#!/bin/sh

# Build object from asm
nasm -o ./linux_x86_shell.o -f elf linux_x86_shell.asm
# create x86 executable from object
ld -m elf_i386 -o linux_x86_shell linux_x86_shell.o 2> /dev/null 
# Extract to machine code
./extract_shellcode.py linux_x86_shell
# Clean up
rm linux_x86_shell.o linux_x86_shell
